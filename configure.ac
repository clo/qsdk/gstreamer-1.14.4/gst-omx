AC_PREREQ(2.60)

dnl please read gstreamer/docs/random/autotools before changing this file

dnl initialize autoconf
dnl releases only do -Wall, git and prerelease does -Werror too
dnl use a three digit version number for releases, and four for git/prerelease
AC_INIT(GStreamer OpenMAX Plug-ins, 0.10.0.1,
    http://bugzilla.gnome.org/enter_bug.cgi?product=GStreamer,
    gst-omx)

AG_GST_INIT

dnl initialize automake
AM_INIT_AUTOMAKE([-Wno-portability 1.10])

dnl define PACKAGE_VERSION_* variables
AS_VERSION

dnl check if this is a release version
AS_NANO(GST_GIT="no", GST_GIT="yes")

dnl can autoconf find the source ?
AC_CONFIG_SRCDIR([omx/gstomx.c])

dnl define the output header for config
AC_CONFIG_HEADERS([config.h])

dnl AM_MAINTAINER_MODE only provides the option to configure to enable it
AM_MAINTAINER_MODE

dnl sets host_* variables
AC_CANONICAL_HOST

dnl use pretty build output with automake >= 1.11
m4_ifdef([AM_SILENT_RULES],[AM_SILENT_RULES([yes])],
  [AM_DEFAULT_VERBOSITY=1
   AC_SUBST(AM_DEFAULT_VERBOSITY)])

dnl our libraries and install dirs use major.minor as a version
GST_MAJORMINOR=$PACKAGE_VERSION_MAJOR.$PACKAGE_VERSION_MINOR
dnl we override it here if we need to for the release candidate of new series
GST_MAJORMINOR=0.10
AC_SUBST(GST_MAJORMINOR)

AG_GST_LIBTOOL_PREPARE

dnl FIXME: this macro doesn't actually work;
dnl the generated libtool script has no support for the listed tags.
dnl So this needs to be fixed first if we want to use this
dnl AS_LIBTOOL_TAGS

AC_LIBTOOL_WIN32_DLL
AM_PROG_LIBTOOL

dnl *** required versions of GStreamer stuff ***
GST_REQ=0.10.35.1

dnl *** autotools stuff ****

dnl allow for different autotools
AS_AUTOTOOLS_ALTERNATE

dnl Add parameters for aclocal
AC_SUBST(ACLOCAL_AMFLAGS, "-I m4 -I common/m4")

dnl *** check for arguments to configure ***

AG_GST_ARG_DEBUG
AG_GST_ARG_PROFILING
AG_GST_ARG_VALGRIND
AG_GST_ARG_GCOV

AG_GST_ARG_EXAMPLES

AG_GST_ARG_WITH_PKG_CONFIG_PATH
AG_GST_ARG_WITH_PACKAGE_NAME
AG_GST_ARG_WITH_PACKAGE_ORIGIN

AG_GST_PKG_CONFIG_PATH

AG_GST_ARG_WITH_PLUGINS

AG_GST_ARG_ENABLE_EXPERIMENTAL

dnl *** checks for platform ***

dnl * hardware/architecture *

dnl common/m4/gst-arch.m4
dnl check CPU type
AG_GST_ARCH

dnl check for large file support
dnl affected plugins must include config.h
AC_SYS_LARGEFILE

dnl *** checks for programs ***

dnl find a compiler
AC_PROG_CC
AC_PROG_CC_STDC

dnl check if the compiler supports '-c' and '-o' options
AM_PROG_CC_C_O

AC_PATH_PROG(VALGRIND_PATH, valgrind, no)
AM_CONDITIONAL(HAVE_VALGRIND, test ! "x$VALGRIND_PATH" = "xno")

dnl check for documentation tools
GTK_DOC_CHECK([1.3])
AS_PATH_PYTHON([2.1])
AG_GST_PLUGIN_DOCS([1.3],[2.1])

dnl *** checks for libraries ***

dnl libm, for sin() etc.
AC_CHECK_LIBM
AC_SUBST(LIBM)

dnl *** checks for header files ***

dnl check if we have ANSI C header files
AC_HEADER_STDC

AX_CREATE_STDINT_H

dnl *** checks for functions ***

dnl *** checks for types/defines ***

dnl *** checks for structures ***

dnl *** checks for compiler characteristics ***

dnl *** checks for library functions ***

dnl Check for a way to display the function name in debug output
AG_GST_CHECK_FUNCTION

dnl *** checks for dependency libraries ***

dnl GLib is required
AG_GST_GLIB_CHECK([2.16])

dnl checks for gstreamer
dnl uninstalled is selected preferentially -- see pkg-config(1)
AG_GST_CHECK_GST($GST_MAJORMINOR, [$GST_REQ], yes)
AG_GST_CHECK_GST_BASE($GST_MAJORMINOR, [$GST_REQ], yes)
AG_GST_CHECK_GST_CONTROLLER($GST_MAJORMINOR, [$GST_REQ], yes)
AG_GST_CHECK_GST_CHECK($GST_MAJORMINOR, [$GST_REQ], no)
AG_GST_CHECK_GST_PLUGINS_BASE($GST_MAJORMINOR, [$GST_REQ], yes)
AM_CONDITIONAL(HAVE_GST_CHECK, test "x$HAVE_GST_CHECK" = "xyes")

dnl Check for documentation xrefs
GLIB_PREFIX="`$PKG_CONFIG --variable=prefix glib-2.0`"
GST_PREFIX="`$PKG_CONFIG --variable=prefix gstreamer-$GST_MAJORMINOR`"
AC_SUBST(GLIB_PREFIX)
AC_SUBST(GST_PREFIX)

dnl Check for external OpenMAX IL headers
AC_CHECK_HEADER([OMX_Core.h], [HAVE_EXTERNAL_OMX=yes], [HAVE_EXTERNAL_OMX=no], [AC_INCLUDES_DEFAULT])
AM_CONDITIONAL(HAVE_EXTERNAL_OMX, test "x$HAVE_EXTERNAL_OMX" = "xyes")

AC_CHECK_DECLS([OMX_VIDEO_CodingVP8],
  [
    AC_DEFINE(HAVE_VP8, 1, [OpenMAX IL has VP8 support])
    HAVE_VP8=yes
  ], [
    HAVE_VP8=no
  ], [[#include <OMX_Video.h>]])
AM_CONDITIONAL(HAVE_VP8, test "x$HAVE_VP8" = "xyes")

dnl Check for -Bsymbolic-functions linker flag used to avoid
dnl intra-library PLT jumps, if available.
AC_ARG_ENABLE(Bsymbolic,
              [AC_HELP_STRING([--disable-Bsymbolic],
                              [avoid linking with -Bsymbolic])],,
              [SAVED_LDFLAGS="${LDFLAGS}"
               AC_MSG_CHECKING([for -Bsymbolic-functions linker flag])
               LDFLAGS=-Wl,-Bsymbolic-functions
               AC_TRY_LINK([], [int main (void) { return 0; }],
                           AC_MSG_RESULT(yes)
                           enable_Bsymbolic=yes,
                           AC_MSG_RESULT(no)
                           enable_Bsymbolic=no)
               LDFLAGS="${SAVED_LDFLAGS}"])

AC_ARG_WITH([omx-target],
        AS_HELP_STRING([--with-omx-target],[Use this OpenMAX IL target (generic, bellagio, rpi)]),
        [ac_cv_omx_target="$withval"], [ac_cv_omx_target="none"])

ac_cv_omx_target_struct_packing="none"
AC_MSG_NOTICE([Using $ac_cv_omx_target as OpenMAX IL target])
case "${ac_cv_omx_target}" in
  generic)
    AC_DEFINE(USE_OMX_TARGET_GENERIC, 1, [Use generic OpenMAX IL target])
    ;;
  rpi)
    AC_DEFINE(USE_OMX_TARGET_RPI, 1, [Use RPi OpenMAX IL target])
    ac_cv_omx_target_struct_packing=4
    ;;
  bellagio)
    AC_DEFINE(USE_OMX_TARGET_BELLAGIO, 1, [Use Bellagio OpenMAX IL target])
    ;;
  none|*)
    AC_ERROR([invalid OpenMAX IL target, you must specify one of --with-omx-target={generic,rpi,bellagio}])
    ;;
esac
AM_CONDITIONAL(USE_OMX_TARGET_GENERIC, test "x$ac_cv_omx_target" = "xgeneric")
AM_CONDITIONAL(USE_OMX_TARGET_BELLAGIO, test "x$ac_cv_omx_target" = "xbellagio")
AM_CONDITIONAL(USE_OMX_TARGET_RPI, test "x$ac_cv_omx_target" = "xrpi")

AC_ARG_WITH([omx-struct-packing],
        AS_HELP_STRING([--with-omx-struct-packing],[Force OpenMAX struct packing, (default is none)]),
        [ac_cv_omx_struct_packing="$withval"], [ac_cv_omx_struct_packing="none"])

if test x"$ac_cv_omx_struct_packing" != x"none"; then
  AC_MSG_NOTICE([Using $ac_cv_omx_struct_packing as OpenMAX struct packing])
  AC_DEFINE_UNQUOTED(GST_OMX_STRUCT_PACKING, $ac_cv_omx_struct_packing, [The struct packing used for OpenMAX structures])
elif test x"$ac_cv_omx_target_struct_packing" != x"none"; then
  AC_MSG_NOTICE([Using $ac_cv_omx_target_struct_packing as OpenMAX struct packing])
  AC_DEFINE_UNQUOTED(GST_OMX_STRUCT_PACKING, $ac_cv_omx_target_struct_packing, [The struct packing used for OpenMAX structures])
fi

dnl *** gst-libs/gst/egl ***
AC_ARG_WITH([egl-window-system],
              AS_HELP_STRING([--with-egl-window-system],[EGL window system to use (x11, mali-fb, rpi, none)]),
              [EGL_WINDOW_SYSTEM="$withval"],
              [EGL_WINDOW_SYSTEM="auto"])

if test x"$EGL_WINDOW_SYSTEM" = x"auto"; then
  dnl Mali
  old_LIBS=$LIBS
  old_CFLAGS=$CFLAGS
  LIBS="$LIBS -lUMP $EGL_LIBS"
  CFLAGS="$CFLAGS $EGL_CFLAGS"
  AC_CHECK_LIB([Mali], [mali_image_create], [EGL_WINDOW_SYSTEM="mali-fb"], [EGL_WINDOW_SYSTEM="auto"])
  LIBS=$old_LIBS
  CFLAGS=$old_CFLAGS

  dnl RPi
  if test x"$EGL_WINDOW_SYSTEM" = x"auto"; then
      old_LIBS=$LIBS
      old_CFLAGS=$CFLAGS
      LIBS="$LIBS -lvcos -lvchiq_arm"
      CFLAGS="$CFLAGS"
      AC_CHECK_LIB([bcm_host], [bcm_host_init],
        [
          LIBS="$LIBS -lbcm_host"
          AC_CHECK_HEADER([bcm_host.h], [EGL_WINDOW_SYSTEM="rpi"], [EGL_WINDOW_SYSTEM="auto"])
      ])
      LIBS=$old_LIBS
      CFLAGS=$old_CFLAGS
  fi

  if test x"$EGL_WINDOW_SYSTEM" = x"auto"; then
      if test x"$HAVE_X11" = x"yes"; then
        EGL_WINDOW_SYSTEM="x11"
      fi
  fi

  if test x"$EGL_WINDOW_SYSTEM" = x"auto"; then
    EGL_WINDOW_SYSTEM="none"
  fi
fi

case "$EGL_WINDOW_SYSTEM" in
  x11|none)
    PKG_CHECK_MODULES(EGL, egl, HAVE_EGL="yes", [
      HAVE_EGL="no"
      old_LIBS=$LIBS
      old_CFLAGS=$CFLAGS
      
      AC_CHECK_LIB([EGL], [eglGetProcAddress],
        [
          AC_CHECK_HEADER([EGL/egl.h],
            [
              HAVE_EGL="yes"
              EGL_LIBS="-lEGL"
              EGL_CFLAGS=""
            ]
          )
        ]
      )
      
      LIBS=$old_LIBS
      CFLAGS=$old_CFLAGS
    ])
      
    if test x"$HAVE_EGL" = x"yes" -a x"$EGL_WINDOW_SYSTEM" = x"x11"; then
      if test x"$HAVE_X11" != x"yes"; then
        AC_MSG_ERROR([libX11 not found and is required for EGL X11 window system])
      else
        AC_DEFINE(USE_EGL_X11, [1], [Use X11 EGL window system])
        EGL_CFLAGS="$EGL_CFLAGS $X11_CFLAGS"
        EGL_LIBS="$EGL_LIBS $X11_LIBS"
      fi
    fi
    ;;
  mali-fb)
    dnl FIXME: Mali EGL depends on GLESv1 or GLESv2
    HAVE_EGL="no"
    old_LIBS=$LIBS
    old_CFLAGS=$CFLAGS
    AC_CHECK_HEADER([EGL/fbdev_window.h],
      [
        LIBS="$LIBS -lUMP"
        AC_CHECK_LIB([Mali], [mali_image_create],
          [
            LIBS="$LIBS -lMali"
            AC_CHECK_LIB([GLESv2], [glEnable],
              [
                AC_CHECK_HEADER([GLES2/gl2.h],
                  [
                    AC_CHECK_LIB([EGL], [eglGetProcAddress],
                      [
                        AC_CHECK_HEADER([EGL/egl.h],
                          [
                            HAVE_EGL="yes"
                            EGL_LIBS="-lGLESv2 -lEGL -lMali -lUMP"
                            EGL_CFLAGS=""
                            AC_DEFINE(USE_EGL_MALI_FB, [1], [Use Mali FB EGL window system])
                          ])
                      ])
                  ])
              ])
          ])
      ])
    LIBS=$old_LIBS
    CFLAGS=$old_CFLAGS
    ;;
  rpi)
    old_LIBS=$LIBS
    old_CFLAGS=$CFLAGS

    dnl FIXME: EGL of RPi depends on GLESv1 or GLESv2
    dnl FIXME: GLESv2 of RPi depends on EGL... WTF!
    LIBS="$LIBS -lvcos -lvchiq_arm"
    AC_CHECK_LIB([bcm_host], [bcm_host_init],
      [
        LIBS="$LIBS -lbcm_host"
        AC_CHECK_HEADER(bcm_host.h,
          [
            LIBS="$LIBS -lGLESv2"
            AC_CHECK_LIB([EGL], [eglGetProcAddress],
              [
                LIBS="$LIBS -lEGL"
                AC_CHECK_HEADER([EGL/egl.h],
                  [
                    AC_CHECK_LIB([GLESv2], [glEnable],
                      [
                        AC_CHECK_HEADER([GLES2/gl2.h],
                          [
                            HAVE_EGL="yes"
                            EGL_LIBS="-lGLESv2 -lEGL -lbcm_host -lvcos -lvchiq_arm"
                            EGL_CFLAGS=""
                            AC_DEFINE(USE_EGL_RPI, [1], [Use RPi EGL window system])
                        ])
                    ])
                ])
            ])
        ])
    ])

    LIBS=$old_LIBS
    CFLAGS=$old_CFLAGS
    ;;
  *)
    AC_MSG_ERROR([invalid EGL window system specified])
    ;;
esac

AC_SUBST(EGL_LIBS)
AC_SUBST(EGL_CFLAGS)
AM_CONDITIONAL(HAVE_EGL, test x"$HAVE_EGL" = x"yes")

dnl *** set variables based on configure arguments ***

dnl set license and copyright notice
GST_LICENSE="LGPL"
AC_DEFINE_UNQUOTED(GST_LICENSE, "$GST_LICENSE", [GStreamer license])
AC_SUBST(GST_LICENSE)

dnl set location of plugin directory
AG_GST_SET_PLUGINDIR

dnl set release date/time
AG_GST_SET_PACKAGE_RELEASE_DATETIME_WITH_NANO([$PACKAGE_VERSION_NANO],
  ["${srcdir}/gst-plugins-base.doap"],
  [$PACKAGE_VERSION_MAJOR.$PACKAGE_VERSION_MINOR.$PACKAGE_VERSION_MICRO])

dnl define an ERROR_CFLAGS Makefile variable
AG_GST_SET_ERROR_CFLAGS($GST_GIT, [
    -Wmissing-declarations -Wmissing-prototypes -Wredundant-decls -Wundef
    -Wwrite-strings -Wformat-nonliteral -Wformat-security -Wformat-nonliteral
    -Winit-self -Wmissing-include-dirs -Waddress -Waggregate-return
    -Wno-multichar -Wnested-externs ])

dnl define correct level for debugging messages
AG_GST_SET_LEVEL_DEFAULT($GST_GIT)

dnl used in examples
AG_GST_DEFAULT_ELEMENTS

dnl *** plug-ins to include ***

dnl these are all the gst plug-ins, compilable without additional libs
AG_GST_CHECK_PLUGIN(omx)

dnl check for gstreamer video base classes
ac_cflags_save="$CFLAGS"
ac_cppflags_save="$CPPFLAGS"
CFLAGS="$CFLAGS $GST_PLUGINS_BASE_CFLAGS $GST_BASE_CFLAGS $GST_CFLAGS"
CPPFLAGS="$CPPFLAGS $GST_PLUGINS_BASE_CFLAGS $GST_BASE_CFLAGS $GST_CFLAGS"

AC_CHECK_HEADER([gst/video/gstvideodecoder.h], [HAVE_VIDEO_BASE_CLASSES="yes"],
  [HAVE_VIDEO_BASE_CLASSES="no"], [AC_INCLUDES_DEFAULT])

CFLAGS="$ac_cflags_save"
CPPFLAGS="$ac_cppflags_save"

if test "x$HAVE_VIDEO_BASE_CLASSES" = "xyes"
then
  AC_MSG_NOTICE([GStreamer base classes for video decoding/encoding found])
  AC_DEFINE_UNQUOTED(HAVE_VIDEO_BASE_CLASSES, 1,
    [defined if GStreamer base classes for video decoding/encoding are present])
else
  AC_MSG_NOTICE([using local copy of GStreamer base classes for video decoding/encoding])
fi
AM_CONDITIONAL(USE_LOCAL_VIDEO_BASE_CLASSES, test "x$HAVE_VIDEO_BASE_CLASSES" = "xno")

dnl check for gstreamer core features (subsystems)
dnl FIXME: this assumes srcdir == builddir for uninstalled setups
GST_CONFIGPATH=`$PKG_CONFIG --variable=includedir gstreamer-0.10`"/gst/gstconfig.h"
AG_GST_PARSE_SUBSYSTEM_DISABLES($GST_CONFIGPATH)

dnl *** finalize CFLAGS, LDFLAGS, LIBS

dnl Overview:
dnl GST_OPTION_CFLAGS:  common flags for profiling, debugging, errors, ...
dnl GST_*:              flags shared by built objects to link against GStreamer
dnl GST_PLUGINS_BASE_CFLAGS: to link internally against the plugins base libs
dnl                          (compare to other modules) or for i18n
dnl GST_ALL_LDFLAGS:    linker flags shared by all
dnl GST_LIB_LDFLAGS:    additional linker flags for all libaries
dnl GST_LT_LDFLAGS:     library versioning of our libraries
dnl GST_PLUGIN_LDFLAGS: flags to be used for all plugins

dnl GST_OPTION_CFLAGS
if test "x$USE_DEBUG" = xyes; then
   PROFILE_CFLAGS="-g"
fi
AC_SUBST(PROFILE_CFLAGS)

if test "x$PACKAGE_VERSION_NANO" = "x1"; then
  dnl Define _only_ when compiling a git version (not pre-releases or releases)
  DEPRECATED_CFLAGS="-DGST_DISABLE_DEPRECATED"
else
  DEPRECATED_CFLAGS=""
fi
AC_SUBST(DEPRECATED_CFLAGS)

dnl every flag in GST_OPTION_CFLAGS and GST_OPTION_CXXFLAGS can be overridden
dnl at make time with e.g. make ERROR_CFLAGS=""
GST_OPTION_CFLAGS="\$(WARNING_CFLAGS) \$(ERROR_CFLAGS) \$(DEBUG_CFLAGS) \$(PROFILE_CFLAGS) \$(GCOV_CFLAGS) \$(OPT_CFLAGS) \$(DEPRECATED_CFLAGS)"
AC_SUBST(GST_OPTION_CFLAGS)

dnl GST_PLUGINS_BASE_CFLAGS
dnl prefer internal headers to already installed ones
dnl also add builddir include for enumtypes and marshal
GST_OMX_CFLAGS=""
AC_SUBST(GST_OMX_CFLAGS)

dnl FIXME: do we want to rename to GST_ALL_* ?
dnl add GST_OPTION_CFLAGS, but overridable
GST_CFLAGS="$GLIB_CFLAGS $GST_CFLAGS $GLIB_EXTRA_CFLAGS \$(GST_OPTION_CFLAGS)"
AC_SUBST(GST_CFLAGS)
dnl add GCOV libs because libtool strips -fprofile-arcs -ftest-coverage
GST_LIBS="$GST_LIBS \$(GCOV_LIBS)"
AC_SUBST(GST_LIBS)

dnl LDFLAGS really should only contain flags, not libs - they get added before
dnl whatevertarget_LIBS and -L flags here affect the rest of the linking
GST_ALL_LDFLAGS="-no-undefined"
if test "x${enable_Bsymbolic}" = "xyes"; then
  GST_ALL_LDFLAGS="$GST_ALL_LDFLAGS -Wl,-Bsymbolic-functions"
fi
AC_SUBST(GST_ALL_LDFLAGS)

dnl this really should only contain flags, not libs - they get added before
dnl whatevertarget_LIBS and -L flags here affect the rest of the linking
GST_PLUGIN_LDFLAGS="-module -avoid-version -export-symbols-regex '^[_]*gst_plugin_desc.*' $GST_ALL_LDFLAGS"
AC_SUBST(GST_PLUGIN_LDFLAGS)

dnl *** output files ***

AC_CONFIG_FILES(
Makefile
gst/Makefile
gst/egl/Makefile
gst/egl/gstreamer-egl-$GST_MAJORMINOR.pc
omx/Makefile
common/Makefile
common/m4/Makefile
tools/Makefile
config/Makefile
config/bellagio/Makefile
config/rpi/Makefile
examples/Makefile
examples/egl/Makefile
)

AC_OUTPUT

